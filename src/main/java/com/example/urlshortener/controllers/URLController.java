package com.example.urlshortener.controllers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import com.example.urlshortener.service.URLConverterService;
import com.example.urlshortener.utils.URLValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;


@RestController
public class URLController {
    private static final Logger LOGGER = LoggerFactory.getLogger(URLController.class);
    private final URLConverterService urlConverterService;
    private final ObjectMapper mapper;

    public URLController(URLConverterService urlConverterService) {
        this.urlConverterService = urlConverterService;
        mapper = new ObjectMapper();
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String testUrl(){
        return "success";
    }

    //returns json object of short and long url
    @RequestMapping(value = "/shortener", method=RequestMethod.POST, consumes = {"application/json"})
    public String shortenUrl(@RequestBody @Valid final ShortenRequest shortenRequest, HttpServletRequest request) throws Exception {
        LOGGER.info("Received url to shorten: " + shortenRequest.getUrl());
        String longUrl = shortenRequest.getUrl();
        if (URLValidator.INSTANCE.validateURL(longUrl)) {
            String localURL = request.getRequestURL().toString();
            String shortenedUrl = urlConverterService.shortenURL(localURL, shortenRequest.getUrl());
            LOGGER.info("Shortened url to: " + shortenedUrl);
             return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(new Response(longUrl,shortenedUrl));
        }
        throw new Exception("Please enter a valid URL");
    }

    // redirect original url if click to shortened url
    @RequestMapping(value = "/{id}", method=RequestMethod.GET)
    public RedirectView redirectUrl(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws IOException, URISyntaxException, Exception {
        LOGGER.info("Received shortened url to redirect: " + id);

        String redirectUrlString = urlConverterService.getLongURLFromID(id);
        LOGGER.info("Original URL: " + redirectUrlString);
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("http://" + redirectUrlString);
        return redirectView;
    }

    @RequestMapping(value = "/getallurls",method = RequestMethod.GET)
    public String getAllUrls() throws JsonProcessingException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(urlConverterService.getAllUrls());
    }
}

class ShortenRequest{
    private String url;

    @JsonCreator
    public ShortenRequest() {

    }

    @JsonCreator
    public ShortenRequest(@JsonProperty("url") String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

class Response{
    private String long_url;
    private String short_url_domain;
    Response(String longUrl, String shortUrl){
        this.long_url = longUrl;
        this.short_url_domain = shortUrl;
    }

    public String getLong_url() {
        return long_url;
    }

    public void setLong_url(String long_url) {
        this.long_url = long_url;
    }

    public String getShort_url_domain() {
        return short_url_domain;
    }

    public void setShort_url_domain(String short_url_domain) {
        this.short_url_domain = short_url_domain;
    }
}

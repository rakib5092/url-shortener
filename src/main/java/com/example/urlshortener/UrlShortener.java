package com.example.urlshortener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlShortener {

    public static void main(final String[] args) {
        System.out.println("Application being started");
        SpringApplication.run(UrlShortener.class, args);
    }

    //  to shortened long to short url

    //    localhost:8080/shortener
    //    json post data:
    //    {
    //        "url":"long url"
    //    }

    // get all urls
    //    localhost:8080/getallurls

//    if click short url it will redirect to long url

}
